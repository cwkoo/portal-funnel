'use strict';

var CLIENT_ID = '436744378085-s0ng5lcr6avva4pj5nj2kmdiaacv4sdk.apps.googleusercontent.com';
var SCOPES = ['https://www.googleapis.com/auth/calendar'];

var DAY_ONE_LETTER_TO_INDEX = {'M': 1, 'T': 2, 'W': 3, 'R': 4, 'F': 5, 'S': 6, 'U': 7};
var DAY_INDEX_TO_TWO_LETTERS = {1: 'MO', 2: 'TU', 3: 'WE', 4: 'TH', 5: 'FR', 6: 'SA', 7: 'SU'};
var SCHOOL_CODES = ['CM', 'HM', 'PO', 'PZ', 'SC'];
var SCHOOL_NAMES = {'CM': 'CMC', 'HM': 'Harvey Mudd', 'PO': 'Pomona', 'PZ': 'Pitzer', 'SC': 'Scripps'};

// Breaks shared by all 5Cs
var COMMON_BREAKS = [
  [1445068800000, 1445414400000], // Fall break
  [1448524800000, 1448870400000], // Thanksgiving
  [1449907200000, 1453190400000], // Winter break
  [1457769600000, 1458547200000], // Spring break
  [1458892800000, 1458979200000], // Cesar Chavez Day
  [1462435200000, 1463472000000] // Spring semester ends
];

// Mudd ends spring semester early
var HM_BREAK = [1462003200000, 1462435200000];

// Pomona and Scripps end fall semester early
var PO_SC_BREAK = [1449734400000, 1449907200000];

// Store local Events
var CAL_EVENTS;

// Store added page elements
var SCHEDULE_ROWS;
var MY_DIV;
var MY_TR;
var TEXT_BEFORE_DROPDOWN;
var CAL_DROPDOWN;
var SPACE_BTWN_DROPDOWN_AND_BUTTON;
var SUBMIT_BUTTON;
var SPACE_BTWN_BUTTON_AND_PROGRESS;
var TEXT_PROGRESS;


// Track progress

// Remaining number of recurring events to process
var EVENTS_LEFT;

// Maps recurring event ID to remaining number of instances to process
var INSTANCES_LEFT;


// Add elements to page
function addElements() {
  removeExistingElements();
  var validated = validatePage();
  if (!validated) {
    return;
  } else {
    var schedule = validated.schedule;
    var groupedGrid = validated.groupedGrid;
  }
  
  var script = document.createElement('script');
  script.id = 'google_client_api';
  script.src = 'https://apis.google.com/js/client.js';
  script.addEventListener('load', checkAuth);
  document.body.appendChild(script);

  MY_DIV = document.createElement('div');
  MY_DIV.id = 'funnel_div';
  MY_DIV.class = 'smalltext';
  MY_DIV.style.padding = '10px';
  MY_DIV.style.marginBottom = '10px';
  MY_DIV.style.backgroundColor = 'PowderBlue';

  var myTd = document.createElement('td');
  myTd.appendChild(MY_DIV);

  MY_TR = document.createElement('tr');
  MY_TR.appendChild(myTd);
  MY_TR.id = 'funnel_tr';

  var tbody = schedule.querySelector('div.pSection>table>tbody');
  var trSchedule = groupedGrid.parentElement.parentElement.parentElement;
  tbody.insertBefore(MY_TR, trSchedule);

  TEXT_BEFORE_DROPDOWN = document.createElement('strong');
  TEXT_BEFORE_DROPDOWN.appendChild(document.createTextNode('Portal Funnel is loading...'));
  MY_DIV.appendChild(TEXT_BEFORE_DROPDOWN);

  CAL_DROPDOWN = document.createElement('select');
  CAL_DROPDOWN.id = 'funnel_dropdown';
  CAL_DROPDOWN.addEventListener('change', onCalSelectionChange);

  SPACE_BTWN_DROPDOWN_AND_BUTTON = document.createElement('div');
  SPACE_BTWN_DROPDOWN_AND_BUTTON.appendChild(document.createTextNode('\u00A0\u00A0\u00A0'));
  
  SUBMIT_BUTTON = document.createElement('button');
  SUBMIT_BUTTON.id = 'funnel_button';
  SUBMIT_BUTTON.type = 'button';
  SUBMIT_BUTTON.textContent = 'Add to calendar!';
  SUBMIT_BUTTON.addEventListener('click', onSubmit);
  SUBMIT_BUTTON.setAttribute('clicked', 'false');
  
  SPACE_BTWN_BUTTON_AND_PROGRESS = document.createElement('div');
  SPACE_BTWN_BUTTON_AND_PROGRESS.appendChild(document.createTextNode('\u00A0\u00A0\u00A0'));
  
  TEXT_PROGRESS = document.createElement('div');
  TEXT_PROGRESS.appendChild(document.createTextNode(''));
  
  [CAL_DROPDOWN, SPACE_BTWN_DROPDOWN_AND_BUTTON, SUBMIT_BUTTON,
      SPACE_BTWN_BUTTON_AND_PROGRESS, TEXT_PROGRESS].forEach(
    function (node) {
      node.style.display = 'none';
    }
  );
  
  MY_DIV.appendChild(CAL_DROPDOWN);
  MY_DIV.appendChild(SPACE_BTWN_DROPDOWN_AND_BUTTON);
  MY_DIV.appendChild(SUBMIT_BUTTON);
  MY_DIV.appendChild(SPACE_BTWN_BUTTON_AND_PROGRESS);
  MY_DIV.appendChild(TEXT_PROGRESS);
}
addElements();

// Alert and quit if on wrong page
function validatePage() {
  var scheduleRows = document.querySelectorAll('div.pt_StudentSchedule table.groupedGrid>tbody>tr');
  var schedule;
  
  if (scheduleRows.length > 0) {
    schedule = document.querySelector('div.pt_StudentSchedule');
  } else {
    // Fall back to old selector if new one doesn't work
    scheduleRows = document.querySelectorAll('div.pi_My_Course_Schedule table.groupedGrid>tbody>tr');
    schedule = document.querySelector('div.pi_My_Course_Schedule');
  }
  
  if (scheduleRows.length === 0) {
    alert('There doesn\'t seem to be a course schedule here.');
    return false;
  }
  
  var groupedGrid = schedule.querySelector('table.groupedGrid');
  
  if (scheduleRows[0].children.length < 8) {
    alert('The course schedule doesn\'t have enough columns. Did you click \"View Details\"?');
    return false;
  } else {
    SCHEDULE_ROWS = scheduleRows;
    return {schedule: schedule, groupedGrid: groupedGrid};
  }
}


// Access Google Calendar API using OAuth

function checkAuth() {
  console.log('Polling page for Google API...');
  (function(){
    if (typeof gapi.auth !== 'undefined') {
      gapi.auth.authorize(
        {'client_id': CLIENT_ID, 'scope': SCOPES, 'immediate': true},
        handleAuthResult);
    } else {
      setTimeout(checkAuth, 50);
    }
  })();
}

function handleAuthResult(authResult) {
  console.log('Handling authorization result...');
  if (authResult && !authResult.error) {
    console.log('Result: Already authorized.');
    loadCalendarApi();
  } else {
    console.log('Result: Not authorized - authorizing...');
    gapi.auth.authorize(
      {'client_id': CLIENT_ID, 'scope': SCOPES, 'immediate': false},
      handleAuthResult);
  }
}

function loadCalendarApi() {
  console.log('Loading Google Calendar API...');
  gapi.client.load('calendar', 'v3', createEvents);
}


// Remove any existing elements created by bookmarklet in this session
function removeExistingElements() {
  var existingMyTr = document.getElementById('funnel_tr');
  var existingScript = document.getElementById('google_client_api');
  if (existingMyTr) {
    console.log('Removing existing elements created by bookmarklet...');
    existingMyTr.parentNode.removeChild(existingMyTr);
    if (existingScript) {
      existingScript.parentNode.removeChild(existingScript);
    }
  }
}

// Create local Events
function createEvents() {
  var courses = parseSchedule();
  CAL_EVENTS = toEvents(courses);
  console.log('Created ' + CAL_EVENTS.length + ' local events.');
  loadCalendarList();
}

// Parse schedule
function parseSchedule() {
  console.log('Parsing schedule...');
  var courses = [];
  for (var rowIndex = 0; rowIndex < SCHEDULE_ROWS.length; rowIndex++) {
    var row = SCHEDULE_ROWS[rowIndex];

    if (row.children.length < 8) {
      continue;
    }

    var id = trimSpaces(row.children[0].children[0].textContent);
    var roomText = row.children[7].children[0].children[0].textContent;
    var school = schoolFromId(id);
    if (school === '' && roomText) {
      school = schoolFromRoom(roomText);
    }

    var title = trimSpaces(row.children[1].textContent);

    var meetNodes = row.children[5].children[0].children;
    var dateNodes = row.children[6].children[0].children;
    var meets = makeMeets(meetNodes, dateNodes);

    var course = new Course(id, school, title, meets);
    courses.push(course);
  }
  console.log('Parsed ' + courses.length + ' rows from schedule.');
  return courses;
}

// Convert Courses to Events
function toEvents(courses) {
  var events = [];
  for (var courseIndex = 0; courseIndex < courses.length; courseIndex++) {
    var eventsThisCourse = courses[courseIndex].makeEvents();
    events.push.apply(events, eventsThisCourse);
  }
  return events;
}

// Load user calendars and show dropdown with calendars
function loadCalendarList() {
  console.log('Loading user calendars...');
  var request = gapi.client.calendar.calendarList.list();
  request.execute(function(resp) {
    var calendars = [];

    for (var calIndex = 0; calIndex < resp.items.length; calIndex++) {
      var calendar = resp.items[calIndex];
      var id = calendar.id;
      var summary = calendar.summary;
      calendars.push([id, summary]);
    }

    // Sort in alphabetical order
    calendars.sort(function(a, b) {
      if (a[1].toUpperCase() < b[1].toUpperCase()) {
        return -1;
      } else if (a[1].toUpperCase() > b[1].toUpperCase()) {
        return 1;
      } else {
        return 0;
      }});

    for (var calIndex = 0; calIndex < calendars.length; calIndex++) {
      var calendarOption = document.createElement('option');
      calendarOption.value = calendars[calIndex][0];
      calendarOption.textContent = calendars[calIndex][1];
      CAL_DROPDOWN.appendChild(calendarOption);
    }

    console.log('Loaded ' + calendars.length + ' user calendars.');
    
    TEXT_BEFORE_DROPDOWN.textContent = 'Select a calendar:\u00A0\u00A0\u00A0';
    [CAL_DROPDOWN, SPACE_BTWN_DROPDOWN_AND_BUTTON, SUBMIT_BUTTON,
        SPACE_BTWN_BUTTON_AND_PROGRESS, TEXT_PROGRESS].forEach(
      function (node) {
        node.style.display = 'initial';
      }
    );
  });
}

// Submit handler: Build insert request
function onSubmit() {
  if (SUBMIT_BUTTON.getAttribute('clicked') === 'true') {
    var toRepeat = confirm('Would you really like to add these courses again?');
    if (!toRepeat) {
      return;
    }
  }

  var calendarId = CAL_DROPDOWN.options[CAL_DROPDOWN.selectedIndex].value;
  MY_DIV.style.backgroundColor = 'LightGrey';
  TEXT_PROGRESS.textContent = 'Added 0 of ' + CAL_EVENTS.length + ' events...';
  SUBMIT_BUTTON.disabled = 'true';
  SUBMIT_BUTTON.setAttribute('clicked', 'true');
  
  EVENTS_LEFT = CAL_EVENTS.length;
  INSTANCES_LEFT = {};
  
  for (var eventIndex = 0; eventIndex < CAL_EVENTS.length; eventIndex++) {
    var ourEvent = CAL_EVENTS[eventIndex];
    var insertRequest = gapi.client.calendar.events.insert({
      'calendarId': calendarId,
      'resource': ourEvent
    });
    insertEvent(insertRequest, ourEvent.school, calendarId);
  }
}

// Insert recurring event
function insertEvent(insertRequest, school, calendarId) {
  insertRequest.execute(function(theirEvent) {
    console.log('Created recurring event ' + theirEvent.summary + ' first starting at ' + theirEvent.start.dateTime);
    onEventInserted(theirEvent, calendarId, school);
  });
}

// Check which instances of inserted events are during breaks
function onEventInserted(theirEvent, calendarId, school) {
  var eventId = theirEvent.id;
  var instancesRequest = gapi.client.calendar.events.instances({
    'calendarId': calendarId,
    'eventId': eventId
  });
  
  instancesRequest.execute(function(resp) {
    INSTANCES_LEFT[eventId] = resp.items.length;
  
    for (var instanceIndex = 0; instanceIndex < resp.items.length; instanceIndex++) {
      var instance = resp.items[instanceIndex];
      var instanceDate = new Date(instance.start.dateTime);

      if (duringBreak(instanceDate, school)) {
        cancelInstance(instance, calendarId, eventId);
      } else {
        doneWith(eventId);
      }
    }
  });
}

// Cancel event instance that's during a break
function cancelInstance(instance, calendarId, eventId) {
  instance.status = ['cancelled'];

  var cancelRequest = gapi.client.calendar.events.update({
    'calendarId': calendarId,
    'eventId': instance.id,
    'resource': instance
  });

  cancelRequest.execute(function (resp) {
    console.log('Canceled instance of ' + instance.summary + ' starting at ' + instance.start.dateTime);
    doneWith(eventId);
  });
}

// Updates progress once all API calls for an event have returned 
function doneWith(eventId) {
  INSTANCES_LEFT[eventId]--;
  if (INSTANCES_LEFT[eventId] === 0) {
    EVENTS_LEFT--;
    TEXT_PROGRESS.textContent = 'Added ' + (CAL_EVENTS.length - EVENTS_LEFT) +
      ' of ' + CAL_EVENTS.length + ' events...';
    console.log((CAL_EVENTS.length - EVENTS_LEFT) + ' events processed.');
    if (EVENTS_LEFT === 0) {
      MY_DIV.style.backgroundColor = 'LightGreen';
      TEXT_PROGRESS.textContent = 'Added all events!';
      SUBMIT_BUTTON.removeAttribute('disabled');
      console.log('All events processed.');
    }
  }
}


// Data structures

function Course(id, school, title, meets, dateRanges) {
  this.id = id;
  this.school = school;
  this.title = title;
  this.meets = meets;
  this.makeEvents = function() {
    var events = [];
    for (var meetIndex = 0; meetIndex < meets.length; meetIndex++) {
      events.push(makeEvent(meets[meetIndex], this.title, school));
    }
    return events;
  };
}

function Meet(days, startTime, endTime, place, startDate, endDate) {
  this.days = days;
  this.startTime = startTime;
  this.endTime = endTime;
  this.place = place;
  this.startDate = startDate;
  this.endDate = endDate;
}

function Time(hour, minute) {
  this.hour = hour;
  this.minute = minute;
}

function Date_(year, month, day) {
  this.year = year;
  this.month = month;
  this.day = day;
}

function Event(summary, location, startDateTime, endDateTime, recurrence, school) {
  this.summary = summary;
  this.location = location;
  this.start = {
    'dateTime': startDateTime,
    'timeZone': 'America/Los_Angeles'
  };
  this.end = {
    'dateTime': endDateTime,
    'timeZone': 'America/Los_Angeles'
  };
  this.recurrence = recurrence;
  this.school = school;
}


// Reactivate submit button when selected calendar changes
function onCalSelectionChange() {
  MY_DIV.style.backgroundColor = 'PowderBlue';
  TEXT_PROGRESS.textContent = '';
  SUBMIT_BUTTON.removeAttribute('disabled');
  SUBMIT_BUTTON.setAttribute('clicked', 'false');
}


// Helper methods

// Get two-letter school code from course ID
function schoolFromId(courseId) {
  var hyphenPos = courseId.indexOf('-');
  var school = courseId.slice(hyphenPos - 2, hyphenPos);
  if (SCHOOL_CODES.indexOf(school) > -1) {
    return school;
  } else {
    return '';
  }
}

// Get two-letter school code from Room column
function schoolFromRoom(roomText) {
  var schoolCodeRegex = /\s*(\S{2})\s*?\//;
  var matches = schoolCodeRegex.exec(roomText);
  if (matches && SCHOOL_CODES.indexOf(matches[1]) > -1) {
    return matches[1];
  } else {
    return '';
  }
}

// Trim spaces
function trimSpaces(input) {
  return input.replace(/\s+/g, ' ').trim();
}

// Parse Meets and Dates columns into an array of Meets
function makeMeets(meetNodes, dateNodes) {
  var meets = [];
  for (var meetIndex = 0; meetIndex < meetNodes.length; meetIndex++) {
    var meetText = meetNodes[meetIndex].textContent.trim();
    var meetRegex = /([A-Z]+)\s+(\d{1,2}):(\d{2})([A-Z]*)-(\d{1,2}):(\d{2})([A-Z]{2}).\s+(.*)/;
    var meetMatches = meetRegex.exec(meetText);
    if (!meetMatches) {
      console.log('Row skipped: couldn\'t parse Meets column.');
      continue;
    }

    var days = [];
    for (var charIndex = 0; charIndex < meetMatches[1].length; charIndex++) {
      days.push(DAY_ONE_LETTER_TO_INDEX[meetMatches[1].charAt(charIndex)]);
    }

    var startHour = parseInt(meetMatches[2], 10);
    var startMinute = parseInt(meetMatches[3], 10);
    var endHour = parseInt(meetMatches[5], 10);
    var endMinute = parseInt(meetMatches[6], 10);
    if ((meetMatches[4] === 'PM' || meetMatches[4] === '' && meetMatches[7] === 'PM') && startHour < 12) {
      startHour += 12;
    }
    if (meetMatches[7] === 'PM' && endHour < 12) {
      endHour += 12;
    }
    var startTime = new Time(startHour, startMinute);
    var endTime = new Time(endHour, endMinute);

    var place = meetMatches[8];

    var dateText = dateNodes[meetIndex].textContent.trim();
    var dateRegex = /(\d{2})\/(\d{2})\/(\d{4})\s+-\s+(\d{2})\/(\d{2})\/(\d{4})/;
    var dateMatches = dateRegex.exec(dateText);
    if (!dateMatches) {
      console.log('Row skipped: couldn\'t parse Dates column.')
    }

    var startDate = new Date_(
      parseInt(dateMatches[3], 10),
      parseInt(dateMatches[1], 10) - 1,
      parseInt(dateMatches[2], 10));
    var endDate = new Date_(
      parseInt(dateMatches[6], 10),
      parseInt(dateMatches[4], 10) - 1,
      parseInt(dateMatches[5], 10));

    var meet = new Meet(days, startTime, endTime, place, startDate, endDate);
    meets.push(meet);
  }
  return meets;
}

// Make an Event
function makeEvent(meet, summary, school) {
  var location = meet.place;
  if (school !== '') {
    location += ' on ' + SCHOOL_NAMES[school];
  }

  var rangeStartDate = meet.startDate;
  var daysUntilClass = untilFirstClass(meet, rangeStartDate);

  var startDateTime = (new Date(Date.UTC(
    rangeStartDate.year,
    rangeStartDate.month,
    rangeStartDate.day + daysUntilClass,
    meet.startTime.hour,
    meet.startTime.minute
  ))).toISOString();
  startDateTime = removeOffset(startDateTime);

  var endDateTime = (new Date(Date.UTC(
    rangeStartDate.year,
    rangeStartDate.month,
    rangeStartDate.day + daysUntilClass,
    meet.endTime.hour,
    meet.endTime.minute
  ))).toISOString();
  endDateTime = removeOffset(endDateTime);

  var rangeEndDate = meet.endDate;
  var untilDateTime = (new Date(Date.UTC(
    rangeEndDate.year,
    rangeEndDate.month,
    rangeEndDate.day + 1,
    8
  ))).toISOString();
  untilDateTime = formatForRecurrence(untilDateTime);

  var twoLetterCodes = [];
  for (var dayIndex = 0; dayIndex < meet.days.length; dayIndex++) {
    twoLetterCodes.push(DAY_INDEX_TO_TWO_LETTERS[meet.days[dayIndex]]);
  }
  var byDay = twoLetterCodes.join();
  var recurrence = [
    'RRULE:FREQ=WEEKLY;UNTIL=' + untilDateTime + ';BYDAY=' + byDay
  ];

  return new Event(summary, location, startDateTime, endDateTime, recurrence, school);
}

// Compute days from when date range starts to when first class meets
function untilFirstClass(meet, rangeStartDate) {
  var rangeStartDay = (new Date(Date.UTC(
    rangeStartDate.year,
    rangeStartDate.month,
    rangeStartDate.day
  ))).getUTCDay();

  var daysToStart = 0;
  while (meet.days.indexOf(rangeStartDay) === -1) {
    if (rangeStartDay === 7) {
      rangeStartDay = 1;
    } else {
      rangeStartDay += 1;
    }
    daysToStart += 1;
  }
  return daysToStart;
}

// Remove the Z (specifies UTC) from a UTC-formatted date-time string
function removeOffset(utcString) {
  if (utcString.charAt(utcString.length - 1) === 'Z') {
    return utcString.slice(0, -1);
  } else {
    return utcString;
  }
}

// Recurrence rule doesn't include hyphens or colons, includes Z
function formatForRecurrence(isoString) {
  return isoString.slice(0, -5).replace(/[-:]/g, '') + 'Z';
}

// Check if event instance is during a break for a given school
function duringBreak(instanceDate, school) {
  for (var breakIndex = 0; breakIndex < COMMON_BREAKS.length; breakIndex++) {
    var break_ = COMMON_BREAKS[breakIndex];
    var breakStart = new Date(break_[0]);
    var breakEnd = new Date(break_[1]);
    if (instanceDate > breakStart && instanceDate < breakEnd) {
      return true;
    }
  }

  if (school === 'HM') {
    return instanceDate > new Date(HM_BREAK[0]) && instanceDate < new Date(HM_BREAK[1]);
  } else if (school === 'PO' || school === 'SC') {
    return instanceDate > new Date(PO_SC_BREAK[0]) && instanceDate < new Date(PO_SC_BREAK[1]);
  } else {
    return false;
  }
}